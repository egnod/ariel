from flask_classful import FlaskView, route, request
from ariel.app import db
from .models import User, UserLoginCard, UserBankCard
from flask_jwt import jwt_required, current_identity


def form_get_or_none(field_name):
    return request.form[field_name] if field_name in request.form else None


class UserView(FlaskView):
    def index(self):
        return "Hello! Get Sign In!"

    def get(self, id: int):
        user = User.query.get(id)

        if not user:
            return "Not Found!"

        return user.to_dict()

    def post(self):
        user = User()
        user.username = request.form["username"]
        user.password_hash = request.form["password"]

        db.session.add(user)
        db.session.commit()

        return ""

    @route("/add/login_card", methods=["POST"])
    @jwt_required()
    def add_login_card(self):
        login_card = UserLoginCard()
        login_card.username = request.form["username"]
        login_card.user_id = current_identity.id
        login_card.password = form_get_or_none("password")
        login_card.site_url = form_get_or_none("site_url")

        db.session.add(login_card)
        db.session.commit()

        return login_card.to_dict()

    @route('/get/login_card/<int:id>', methods=["GET"])
    @jwt_required()
    def get_login_card(self, id: int):
        login_card = UserLoginCard.query.get(id)

        if not login_card:
            return "Not Found!"

        return login_card.to_dict() if login_card.user_id == current_identity.id else "Permission deny!"

    @route('/add/bank_card', methods=['POST'])
    @jwt_required()
    def add_bank_card(self):
        bank_card = UserBankCard()
        bank_card.user_id = current_identity.id
        bank_card.card_number = request.form['cardnumber']
        bank_card.fullname_issuer = request.form['fullnameissuer']
        bank_card.cvc_code = request.form['cvccode']
        bank_card.expire_date = request.form['expiredate']

        db.session.add(bank_card)
        db.session.commit()

        return bank_card.to_dict()

    @route('/get/bank_card/<int:id>', methods=['GET'])
    @jwt_required()
    def get_bank_card(self, id: int):
        bank_card = UserBankCard.query.get(id)

        if not bank_card:
            return 'Not Found!'

        return bank_card.to_dict() if bank_card.user_id == current_identity.id else 'Permission dany!'
