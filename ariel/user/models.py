from ..app import db
from sqlalchemy.ext.hybrid import hybrid_property
from slugify import slugify
from passlib.hash import pbkdf2_sha256
from uuid import uuid4
import base64


class User(db.Model):
    __tablename__ = 'users'

    _username = db.Column("username", db.String, nullable=False, unique=True)
    _password_hash = db.Column("password_hash", db.String, nullable=False)
    uuid = db.Column(db.String, default=lambda: uuid4())

    def to_dict(self):
        return {
            "username": self.username,
            "password_hash": self.password_hash,
            "uuid": self.uuid,
            "id": self.id
        }

    @hybrid_property
    def username(self):
        return self._username

    @username.setter
    def username(self, value):
        self._username = slugify(value)

    @hybrid_property
    def password_hash(self):
        return self._password_hash

    @password_hash.setter
    def password_hash(self, value):
        self._password_hash = pbkdf2_sha256.hash(value)

    def check_password(self, password):
        return pbkdf2_sha256.verify(password, self._password_hash)

    @classmethod
    def _bootstrap(cls, count=1):
        from mimesis.providers import Person

        p = Person(locale="en")

        for _ in range(count):
            user = cls()
            user.username = p.name()
            user.password_hash = p.password()

            db.session.add(user)
        db.session.commit()


class UserLoginCard(db.Model):
    _username = db.Column("username", db.String, nullable=False)
    _password = db.Column("password", db.String)
    site_url = db.Column(db.String)

    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    user = db.relationship(User)

    @hybrid_property
    def password(self):
        if self._password and isinstance(self._password, str):
            return base64.b64decode(self._password.encode()).decode()
        else:
            return self._password

    @password.setter
    def password(self, value):
        if value and isinstance(value, str):
            self._password = base64.b64encode(value.encode()).decode()

    @hybrid_property
    def username(self):
        return base64.b64decode(self._username.encode()).decode()

    @username.setter
    def username(self, value):
        self._username = base64.b64encode(value.encode()).decode()

    def to_dict(self):
        return {
            "id": self.id,
            "username": self.username,
            "password": self.password,
            "site_url": self.site_url,
            "user": self.user.to_dict()
        }


class UserBankCard(db.Model):
    fullname_issuer = db.Column(db.String, nullable=False)
    _card_number = db.Column("card_number", db.String, nullable=False)
    expire_date = db.Column(db.String, nullable=False)
    cvc_code = db.Column(db.Integer, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    user = db.relationship(User)

    @hybrid_property
    def card_number(self):
        if self._card_number and isinstance(self._card_number, str):
            return base64.b64decode(self._card_number.encode()).decode()
        else:
            return self._card_number

    @card_number.setter
    def card_number(self, value):
        if value and isinstance(value, str):
            value = value.replace(" ", "")

            d = list(str(value))
            nechet = []
            chet = []

            for x in range(0, len(d), 2):
                c = int(d[x]) * 2
                if c > 9:
                    nechet.append(c // 10)
                    nechet.append(c % 10)
                else:
                    nechet.append(c)

            for x in range(1, len(d), 2):
                chet.append(int(d[x]))

            i = sum(chet) + sum(nechet)
            if i % 10 == 0:
                self._card_number = base64.b64encode(value.encode()).decode()

    def to_dict(self):
        return {
            "id": self.id,
            "fullname_issuer": self.fullname_issuer,
            "card_number": self.card_number,
            "expire_date": self.expire_date,
            "cvc_code": self.cvc_code,
            "user": self.user.to_dict()
        }
