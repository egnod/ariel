import boto3
from ariel.app.config.storage import *


def create_storage_client():
    session = boto3.session.Session()

    s3 = session.client(
        service_name="s3",
        region_name=S3_REGION,
        endpoint_url=S3_HOST,
        aws_secret_access_key=S3_ACCESS_SECRET_KEY,
        aws_access_key_id=S3_ACCESS_KEY_ID
    )

    return s3
