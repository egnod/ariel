from os import getenv

DB_NAME = getenv("ARIEL_DB_NAME", "ariel")
DB_USER = getenv("ARIEL_DB_USER", "ariel")
DB_PASSWROD = getenv("ARIEL_DB_PASSWORD", "ariel_password")
DB_HOST = getenv("ARIEL_DB_HOST", "localhost")
DB_PORT = getenv("ARIEL_DB_PORT", "5432")


SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://{DB_USER}:{DB_PASSWROD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
SQLALCHEMY_TRACK_MODIFICATIONS = False

MIGRATIONS_DIR = "ariel/app/migrations/"
