from os import urandom
from binascii import hexlify

SECRET_KEY = hexlify(urandom(8))