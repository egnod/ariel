from ariel.user.models import User


def auth(username, password):
    user = User.query.filter_by(username=username).first()

    if user and user.check_password(password):
        return user


def identity(payload):
    user_id = int(payload["identity"])
    return User.query.get(user_id)
