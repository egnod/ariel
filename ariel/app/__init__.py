from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_debugtoolbar import DebugToolbarExtension
from flask_jwt import JWT
from .mixin import IdModel
from .config import MIGRATIONS_DIR

db = SQLAlchemy(model_class=IdModel)
migrate = Migrate(directory=MIGRATIONS_DIR)

toolbar = DebugToolbarExtension()

from .models import *
from .auth import auth, identity
jwt = JWT(authentication_handler=auth, identity_handler=identity)

def create_app():
    app = Flask(__name__, template_folder="templates/")
    app.config.from_pyfile("cfg.py")
    app.debug = True

    db.init_app(app)
    migrate.init_app(app, db)
    toolbar.init_app(app)
    jwt.init_app(app)

    from .views import register_views

    register_views(app)

    return app

