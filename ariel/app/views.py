
def register_views(app):
    from ariel.user.views import UserView

    UserView.register(app)
